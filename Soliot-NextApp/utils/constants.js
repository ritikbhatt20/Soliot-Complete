import { PublicKey } from "@solana/web3.js";

export const REGISTRY_SEED = "registry";
export const NODE_SEED = "node";

export const PROGRAM_ID = new PublicKey(            
    "3mgBjeppjDsFkRfDzqM2hHEJbeu5ddjFghH9mqdwNgs8"    
);                                                      